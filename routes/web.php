<?php

use Illuminate\Support\Facades\{Route, Auth};

use App\Http\Controllers\{
    AboutController,
    CategoryController,
    ContactController,
    CourseController,
    HomeController,
    PolicyController,
    RazorpayController,
    SliderController
};
use App\Http\Livewire\Checkout;

Route::get('/', [HomeController::class, 'index'])->name('welcome');
Route::get('about', [HomeController::class, 'about'])->name('about');
Route::get('contact', [HomeController::class, 'contact'])->name('contact');


Auth::routes();

Route::post('contact', [ContactController::class, 'contact'])->name('contact');


Route::get('course-details/{course:slug}', [CourseController::class, 'courseDetail'])->name('course.details');
Route::get('my-courses', [CourseController::class, 'userCourse'])->name('user.courses');

Route::get('terms-and-conditions', [HomeController::class, 'terms'])->name('terms');

Route::middleware('auth')->group(function () {

    Route::get('checkout/{course:slug}', [RazorpayController::class, 'index'])->name('razorpay.index');
    Route::post('checkout/{course:slug}', [RazorpayController::class, 'store'])->name('razorpay.store');

    Route::get('show', [RazorpayController::class, 'pdf_show'])->name('razorpay.show');

    Route::middleware('admin')->prefix('admin')->group(function () {
        Route::get('home', [HomeController::class, 'home'])->name('home');
        Route::post('about-image-slider', [AboutController::class, 'aboutSliderStore'])->name('about.slider.store');
        Route::delete('about-image-slider/{id}/delete', [AboutController::class, 'aboutSliderDestroy'])->name('about.slider.destroy');

        Route::get('about-image', [AboutController::class, 'index'])->name('about.index');
        Route::post('about-image', [AboutController::class, 'store'])->name('about.store');
        Route::delete('about-image/{id}/delete', [AboutController::class, 'destroy'])->name('about.destroy');

        Route::get('slider-image', [SliderController::class, 'index'])->name('slider.index');
        Route::post('slider-image', [SliderController::class, 'store'])->name('slider.store');
        Route::delete('slider-image/{id}/delete', [SliderController::class, 'destroy'])->name('slider.destroy');

        Route::get('category', [CategoryController::class, 'index'])->name('category.index');
        Route::post('category', [CategoryController::class, 'store'])->name('category.store');
        Route::get('category/create', [CategoryController::class, 'create'])->name('category.create');
        Route::get('category/{category:id}/edit', [CategoryController::class, 'edit'])->name('category.edit');
        Route::put('category/{category:slug}', [CategoryController::class, 'update'])->name('category.update');
        Route::delete('category/{category:slug}/delete', [CategoryController::class, 'destroy'])->name('category.destroy');

        Route::resource('course', CourseController::class);

        Route::get('course-users/{id}', [CourseController::class, 'courseUsers'])->name('course.users');
        Route::delete('course-user/{id}/delete', [CourseController::class, 'courseUsersDelete'])->name('course.user.destroy');

        Route::post('course-services', [CourseController::class, 'serviceStore'])->name('course.service');
        Route::delete('course-services/{service:id}/delete', [CourseController::class, 'serviceDelete'])->name('service.destroy');

        Route::delete('contact/{id}/delete', [ContactController::class, 'destroy'])->name('contact.destroy');

        Route::get('terms', [PolicyController::class, 'index'])->name('policy.index');
        Route::post('terms', [PolicyController::class, 'store'])->name('policy.store');
        Route::get('terms/create', [PolicyController::class, 'create'])->name('policy.create');
        Route::get('terms/{policy:id}/edit', [PolicyController::class, 'edit'])->name('policy.edit');
        Route::put('terms/{policy:id}', [PolicyController::class, 'update'])->name('policy.update');
        Route::delete('terms/{policy:id}/delete', [PolicyController::class, 'destroy'])->name('policy.destroy');
    });
});
