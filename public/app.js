new Swiper("#mySwiper", {
    spaceBetween: 30,
      navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
      },
      speed: 5000,
      keyboard: {
        enabled: true,
      },
      pagination: {
        el: ".swiper-pagination",
        clickable:true
      },
  slidesPerView: 'auto',
      loop: true,
      autoplay: {
        delay: 8000,
        disableOnInteraction: true,
      },
    });

    setTimeout("openPopup()", 15000); // after 3 seconds


    function openPopup() {
      document.getElementById('modified_popup').classList.add("popup_active");
    }

    function closePopup() {
      document.getElementById('modified_popup').classList.remove("popup_active");
    }

    document.getElementById('year').innerHTML = new Date().getFullYear()
