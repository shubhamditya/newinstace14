<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Admin',
            'is_admin' => true,
            'email' => 'admin@admin.com',
            'phone_number' => '1234567890',
            'password' => bcrypt('Gdm@1234567890'),
        ]);

        Category::create([
            'name' => 'Programming',
            'slug' => 'programming'
        ]);

        // \App\Models\User::factory(10)->create();
    }
}
