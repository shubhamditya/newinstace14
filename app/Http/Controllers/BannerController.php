<?php

namespace App\Http\Controllers;

use App\Models\BannerImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class BannerController extends Controller
{
    public function index()
    {
        $banners = BannerImage::all();
        return view('Admin.banner.index', compact('banners'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'image' => 'required',
            'title' => 'required',
            'description' => 'required',
        ]);

        $imageName = time() . '-' . $request->image->getClientOriginalName();

        $request->image->move(public_path() . '/uploads/', $imageName);

        BannerImage::create([
            'title' => $request->title,
            'description' => $request->description,
            'image' => $imageName,
            'is_video' => $request->has('is_video')
        ]);

        return back()->with('status', 'Image Uploaded Successfully');
    }

    public function destroy($id)
    {
        $banner = BannerImage::find($id);

        File::delete(public_path() . '/uploads/' . $banner->image);

        $banner->delete();

        return back()->with('status', 'Image Deleted Successfully');
    }
}
