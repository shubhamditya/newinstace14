<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\CourseAdmission;
use Illuminate\Http\Request;
use Razorpay\Api\Api;
use PDF;

class RazorpayController extends Controller
{
    public function index(Course $course)
    {
        $gst_price = ($course->price) * 1.18;

        $gst_price_only = $gst_price - $course->price;

        return view('checkout', compact('course', 'gst_price', 'gst_price_only'));
    }

    /**
     * Write code on Method
     *
     * @return response()
     */
    public function store(Request $request, Course $course)
    {
        $input = $request->all();

        $api = new Api(env('RAZORPAY_KEY'), env('RAZORPAY_SECRET'));

        $payment = $api->payment->fetch($input['razorpay_payment_id']);

        if (count($input)  && !empty($input['razorpay_payment_id'])) {
            try {
                $response = $api->payment->fetch($input['razorpay_payment_id'])->capture(array('amount' => $payment['amount']));
            } catch (\Exception $e) {
                return back()->with('error', $e->getMessage());
            }
        }

        CourseAdmission::create([
            'user_id' => auth()->id(),
            'course_id' => $course->id,
            'payment_id' => $payment->id
        ]);

        // return redirect()->route('razorpay.show');
        return redirect()->route('course.details', $course->slug)->with('status', 'Congratulation, Payment Successfully Done');
    }

    public function pdf_show()
    {
        $data = [
            'title' => 'This is a Receipt.',
            'date' => now()
        ];

        $pdf = PDF::loadView('pdf.receipt', $data);

        return $pdf->download('receipt.pdf');
    }
}
