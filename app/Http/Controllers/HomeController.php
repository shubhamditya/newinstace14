<?php

namespace App\Http\Controllers;

use App\Models\AboutBanner;
use App\Models\AboutSlider;
use App\Models\BannerImage;
use App\Models\Contact;
use App\Models\Course;
use App\Models\Policy;
use App\Models\SliderImage;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $sliders = SliderImage::all();

        $courses = Course::all();

        return view('welcome', compact('sliders', 'courses'));
    }

    public function about()
    {
        $about_banners = AboutBanner::all();

        $about_sliders = AboutSlider::all();

        return view('about', compact('about_banners', 'about_sliders'));
    }

    public function contact()
    {
        return view('contact');
    }

    public function home()
    {
        $contacts = Contact::latest()->get();

        return view('home', compact('contacts'));
    }

    public function terms()
    {
        $terms = Policy::all();

        return view('terms', compact('terms'));
    }
}
