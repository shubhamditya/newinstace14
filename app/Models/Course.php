<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'slug',
        'category_id',
        'thumbnail',
        'demo_video',
        'excerpt',
        'description',
        'price',
        'discount_price',
        'batch_start_at',
    ];

    protected $casts = [
        'price' => 'integer',
        'discount_price' => 'integer',
        'batch_start_at' => 'date'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function services()
    {
        return $this->hasMany(Service::class);
    }

    public function setPriceAttribute($value)
    {
        $this->attributes['price'] = $value * 100;
    }

    public function setDiscountPriceAttribute($value)
    {
        $this->attributes['discount_price'] = $value * 100;
    }
}
