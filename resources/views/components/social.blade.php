<ul class="social-links shadow" id="social_link">
    <li>
        <a target="_blank" href="https://www.facebook.com/Magical-Umbrella-Pvt-Ltd-109325971620028"><img
                src="{{ asset('images/social/facebook.png') }}" /></a>
    </li>
    <li>
        <a target="_blank" href="https://www.instagram.com/magicalumbrella_mu/"><img
                src="{{ asset('images/social/instagram.png') }}" /></a>
    </li>
    {{-- <li>
        <a target="_blank" href="https://twitter.com/Magical_MU"><img src="{{ asset('images/social/twitter.png') }}" /></a>
    </li> --}}
    {{-- <li>
        <a target="_blank" href="https://www.linkedin.com/in/magical-umbrella-b15bb0227/"><img src="{{ asset('images/social/linkedin.png') }}" /></a>
    </li> --}}
</ul>
