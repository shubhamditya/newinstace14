<div class="term_dot">
    <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 172 172" style=" fill:#000000;">
        <g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" stroke-linejoin="miter"
            stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" font-family="none" font-weight="none"
            font-size="none" text-anchor="none" style="mix-blend-mode: normal">
            <path d="M0,172v-172h172v172z" fill="none"></path>
            <g fill="#ffffff">
                <path
                    d="M57.34733,28.65267c-15.83117,0 -28.66667,12.8355 -28.66667,28.66667v57.33333c0,15.83117 12.8355,28.66667 28.66667,28.66667h57.33333c15.83117,0 28.66667,-12.8355 28.66667,-28.66667v-57.33333c0,-15.83117 -12.8355,-28.66667 -28.66667,-28.66667z">
                </path>
            </g>
        </g>
    </svg>
</div>
