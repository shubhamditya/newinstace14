@extends('layouts.admin')

@section('content')
    <div class="container">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        <div class="row">
            <div class="col-md-10">
                <div style="display: flex;justify-content: flex-end">
                    <a href="{{ route('course.create') }}" class="btn btn-primary">
                        Add Courses
                    </a>
                </div>
            </div>
        </div>
        <div class="row mt-4 justify-content-center">
            <div class="col-md-9">
                <table class="table">
                    <thead class="table-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Thumbnail</th>
                            <th scope="col">Title</th>
                            <th scope="col">Category</th>
                            <th scope="col">Price</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($courses as $course)
                            <tr>
                                <th scope="row">{{ $loop->iteration }}</th>
                                <th><img src="{{ asset('storage/' . $course->thumbnail) }}" width="100px" alt=""></th>
                                <th>{{ $course->title }}</th>
                                <th>{{ $course->category->name }}</th>
                                <th>Rs. {{ $course->price / 100 }} /-</th>
                                <td class="d-flex justify-content-center">
                                    <a href="{{ route('course.users', $course->id) }}"
                                        class="btn btn-warning mr-2 text-white">Interns</a>
                                    <a href="{{ route('course.edit', $course->id) }}" class="btn btn-info mr-2">Edit</a>
                                    <form action="{{ route('course.destroy', $course->id) }}" method="post">
                                        @csrf
                                        @method('delete')
                                        <button class="btn btn-danger">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
