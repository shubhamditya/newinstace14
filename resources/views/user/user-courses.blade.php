@extends('layouts.base')

@section('title')
    My Courses
@endsection

@section('content')
    <section class="container-fluid max_width" id="courses" style="min-height: 87.5vh">
        <div class="row m-0">
            <div class="course_title pt-3">
                <h2>My Courses</h2>
            </div>
            @foreach ($courses as $course)
                <div class="col-md-4 col-sm-6 mt-4">
                    <div class="card card-manage">
                        <a href="{{ route('course.details', $course->slug) }}">
                            <img src="{{ asset('storage/' . $course->thumbnail) }}" class="card-img-top"
                                alt="{{ $course->title }}" height="200">
                        </a>
                        <div class="card-body">
                            <a href="{{ route('course.details', $course->slug) }}">
                                <h5 class="card-title">{{ $course->title }}</h5>
                            </a>
                            <p class="card-text">{{ $course->excerpt }}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </section>
@endsection

@section('scripts')
    <script src="{{ asset('app.js') }}"></script>
@endsection
